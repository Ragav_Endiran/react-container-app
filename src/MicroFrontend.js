import React, { useEffect } from 'react'
function MicroFrontend({ name, host }) {
    useEffect(() => {
        // create ID for each MFE 
        const scriptId = `micro-frontend-script-${name}`;
        // render function for  MFE 
        const renderMicroFrontend = () => {
            window[`render${name}`](`${name}-container`);
        }
        // if ID is already present in the DOM, the following function will excute
        if (document.getElementById(scriptId)) {
            renderMicroFrontend();
            return;
        }
        /***********************************
        if ID is not present, the following will create a ID with MFE script file
         from asset-manifest.json
         ************************/
        fetch(`${host}/asset-manifest.json`)
            .then(res => res.json())
            .then(manifest => {
                const src = `${host}${manifest.files["main.js"]}`;
                console.log(src)
                const script = document.createElement('script');
                script.id = scriptId;
                script.crossOrigin = "";
                script.src = src;
                script.onload = () => {
                    renderMicroFrontend();
                };
                document.head.appendChild(script);
            });
        // unmount function to remove MFE from DOM
        return () => {
            window[`unmount${name}`] && window[`unmount${name}`](scriptId);
        };
    });
    return <main id={`${name}-container`}></main>
}
MicroFrontend.defaultProps = {
    document,
    window
}
export default MicroFrontend;
