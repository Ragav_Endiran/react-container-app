import './App.css';
import MicroFrontend from './MicroFrontend';

const {
  REACT_APP_ONE: appOne,
  REACT_APP_TWO: appTwo
} = process.env;
function App() {
  return (
    <div className="App">
      <h2>Button project!</h2>
      <div style={{ display: 'flex', justifyContent: 'space-evenly' }}>
        <MicroFrontend host={appOne} name="AppOne" />
        <MicroFrontend host={appTwo} name="AppTwo" />
      </div>
    </div>
  );
}
export default App;
